#!/usr/bin/env python

from distutils.core import setup

setup(
    name='face_fit',
    version='0.1',
    packages=['face_fit']
)
