import bpy
from .operator_handlers import \
    LoadSceneOperatorHandler, \
    RefineFaceTransformOperatorHandler, \
    VisualizeDistanceOperatorHandler

bl_info = {
    'name': 'Face fit',
    'description': 'Face fit tools for blender',
    'author': '',
    'version': (0, 0, 1),
    'blender': (2, 76, 0),
    'location': 'Properties > Scene',
    'category': 'Development'
}


class FaceFitSettings(bpy.types.PropertyGroup):
    tensor_path_prop = \
        bpy.props.StringProperty(name='Tensor Path', subtype='FILE_PATH')
    pose_path_prop = \
        bpy.props.StringProperty(name='Pose Path', subtype='FILE_PATH')
    jpg_path_prop = \
        bpy.props.StringProperty(name='Image Path', subtype='FILE_PATH')
    mesh_path_prop = \
        bpy.props.StringProperty(name='Mesh Path', subtype='FILE_PATH')


class FaceFitData(bpy.types.PropertyGroup):
    object_table = dict()
    scene_loaded = \
        bpy.props.BoolProperty(name="Scene loaded")


class LoadSceneOperator(bpy.types.Operator):
    bl_idname = "scene.face_fit_load"
    bl_label = "Load Face Fit Scene"
    handler = LoadSceneOperatorHandler()

    def execute(self, context):
        self.handler.handle()
        bpy.context.scene.face_fit_data.scene_loaded = True
        return {'FINISHED'}


class RefineFaceTransformOperator(bpy.types.Operator):
    bl_idname = "scene.face_fit_refine_face_transform"
    bl_label = "Refine Face Rigid Transform"
    handler = RefineFaceTransformOperatorHandler()

    def execute(self, context):
        if bpy.context.scene.face_fit_data.scene_loaded:
            self.handler.handle()
        return {'FINISHED'}


class VisualizeDistanceOperator(bpy.types.Operator):
    bl_idname = "scene.face_fit_visualize_distance"
    bl_label = "Visualize Distance"
    handler = VisualizeDistanceOperatorHandler()

    def execute(self, context):
        if bpy.context.scene.face_fit_data.scene_loaded:
            self.handler.handle()
        return {'FINISHED'}


class FaceFitPanel(bpy.types.Panel):
    bl_idname = 'SCENE_PT_face_fit'
    bl_label = 'Face Fit'
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'scene'

    @classmethod
    def poll(self, context):
        return context.scene is not None

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        face_fit_prop = scene.face_fit_prop

        layout.label(text='Paths to input data:')
        layout.prop(face_fit_prop, 'tensor_path_prop')
        layout.prop(face_fit_prop, 'pose_path_prop')
        layout.prop(face_fit_prop, 'jpg_path_prop')
        layout.prop(face_fit_prop, 'mesh_path_prop')
        layout.separator()
        layout.label(text='Operators:')
        layout.operator('scene.face_fit_load')
        layout.operator('scene.face_fit_refine_face_transform')
        layout.operator('scene.face_fit_visualize_distance')


def register():
    bpy.utils.register_module(__name__)
    bpy.types.Scene.face_fit_prop = \
        bpy.props.PointerProperty(type=FaceFitSettings)
    bpy.types.Scene.face_fit_data = \
        bpy.props.PointerProperty(type=FaceFitData)


def unregister():
    bpy.utils.unregister_module(__name__)
    del bpy.types.Scene.face_fit_prop
    del bpy.types.Scene.face_fit_data


if __name__ == '__main__':
    register()
