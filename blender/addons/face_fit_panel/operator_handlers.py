import json

import bpy

import numpy as np
from scipy.spatial import KDTree

import face_fit.utils.wavefront as obj_utils
import face_fit.utils.depth as depth_utils
from face_fit.utils.projection import FullProjection
from face_fit.minimizer.pose_minimizer import refine_head_position_landmarks

from .utils import create_bpy_item, apply_transforms_from_obj, make_empty
from .scene import \
    make_depth_map_mesh,\
    make_landmarks,\
    make_ARKit_face,\
    make_ARKit_landmarks


class OperatorHandler(object):

    def prepare_data(self):
        pass

    def process_data(self):
        pass

    def apply_result(self):
        pass

    def handle(self):
        self.prepare_data()
        self.process_data()
        self.apply_result()


class LoadSceneOperatorHandler(OperatorHandler):

    def __init__(self):
        self._tensor_path = None
        self._pose_path = None
        self._mesh_path = None
        self._jpeg_path = None
        self._loaded_objects = {}

    def prepare_data(self):
        face_fit_prop = bpy.context.scene.face_fit_prop
        self._tensor_path = face_fit_prop.tensor_path_prop
        self._pose_path = face_fit_prop.pose_path_prop
        self._mesh_path = face_fit_prop.mesh_path_prop
        self._jpeg_path = face_fit_prop.jpg_path_prop

    def process_data(self):
        with open(self._tensor_path, 'r') as f:
            tensor = json.load(f)
        with open(self._pose_path, 'r') as f:
            pose = json.load(f)
        with open(self._mesh_path, 'r') as f:
            mesh = obj_utils.load(f)
        depth_map = depth_utils.read_depth_map(self._jpeg_path)

        projection_builder = FullProjection.Builder

        self._loaded_objects['depth'] = \
            make_depth_map_mesh(pose, depth_map, projection_builder)
        self._loaded_objects['depth_landmarks'] = \
            make_landmarks(pose, depth_map, projection_builder)
        self._loaded_objects['face'] = \
            make_ARKit_face(pose, mesh)
        self._loaded_objects['face_landmarks'] = \
            make_ARKit_landmarks(tensor, pose, mesh)

    def apply_result(self):
        assert 'face' in self._loaded_objects and \
               'face_landmarks' in self._loaded_objects and \
               'depth' in self._loaded_objects and \
               'depth_landmarks' in self._loaded_objects

        face_fit_data = bpy.context.scene.face_fit_data

        # add name reference to table
        for (key, value) in self._loaded_objects.items():
            face_fit_data.object_table[key] = value['name']

        face_fit_obj = make_empty("FaceFit", children=self._loaded_objects.values())
        create_bpy_item(face_fit_obj)


class RefineFaceTransformOperatorHandler(OperatorHandler):

    def __init__(self):
        self._face = None
        self._depth_landmarks = None
        self._face_landmarks = None
        self._new_location = None
        self._new_rotation_quat = None
        self._new_scale = None

    def prepare_data(self):
        face_fit_data = bpy.context.scene.face_fit_data

        face_obj_name = \
            face_fit_data.object_table['face']
        face_landmarks_obj_name = \
            face_fit_data.object_table['face_landmarks']
        depth_landmarks_obj_name = \
            face_fit_data.object_table['depth_landmarks']

        self._depth_landmarks = bpy.data.objects[depth_landmarks_obj_name]
        self._face_landmarks = bpy.data.objects[face_landmarks_obj_name]
        self._face = bpy.data.objects[face_obj_name]

    def process_data(self):

        depth_landmarks = self._depth_landmarks.children
        face_landmarks = self._face_landmarks.children

        depth_landmarks_indices = \
            [int(item.name[-2:]) for item in depth_landmarks]
        face_landmarks_indices = \
            [int(item.name[-2:]) for item in face_landmarks]

        assert len(face_landmarks_indices) - 1 == max(face_landmarks_indices)

        depth_landmarks_vertices = \
            [np.array(item.location[:]) for item in depth_landmarks]
        face_landmarks_vertices = \
            [np.array(item.location[:]) for item in face_landmarks]

        depth_landmarks_vertices = \
            apply_transforms_from_obj(
                depth_landmarks_vertices,
                self._depth_landmarks
            )

        if len(depth_landmarks_indices) < len(face_landmarks_indices):
            missing_indices = \
                set(face_landmarks_indices) - set(depth_landmarks_indices)
            missing_indices = list(missing_indices)
            for index in missing_indices:
                depth_landmarks_vertices = np.insert(
                    depth_landmarks_vertices,
                    index,
                    np.nan,
                    axis=0
                )

        # discard contour landmarks
        indices = set(depth_landmarks_indices) - set(range(0, 17))
        indices = list(indices)

        head_translation = \
            np.array(self._face_landmarks.location[:])
        head_rotation_quat = \
            np.array(self._face_landmarks.rotation_quaternion[:])

        result = refine_head_position_landmarks(
            head_landmarks=face_landmarks_vertices,
            reference_landmarks=depth_landmarks_vertices,
            indices=indices,
            initial_translation=head_translation,
            initial_rotation_quat=head_rotation_quat
        )

        self._new_rotation_quat = result[0].tolist()
        self._new_location = result[1].tolist()
        self._new_scale = [result[2], result[2], result[2]]

    def apply_result(self):
        self._face.location = self._new_location
        self._face.rotation_quaternion = self._new_rotation_quat
        self._face.scale = self._new_scale

        self._face_landmarks.location = self._new_location
        self._face_landmarks.rotation_quaternion = self._new_rotation_quat
        self._face_landmarks.scale = self._new_scale


class VisualizeDistanceOperatorHandler(OperatorHandler):

    def __init__(self):
        self._depth_obj = None
        self._face_obj = None
        self._distance = None

    def prepare_data(self):
        face_fit_data = bpy.context.scene.face_fit_data
        face_obj_name = face_fit_data.object_table['face']
        depth_obj_name = face_fit_data.object_table['depth']

        self._depth_obj = bpy.data.objects[depth_obj_name]
        self._face_obj = bpy.data.objects[face_obj_name]

    def process_data(self):

        depth_vertices = \
            list(map(lambda v: v.co[:], self._depth_obj.data.vertices))
        face_vertices = \
            list(map(lambda v: v.co[:], self._face_obj.data.vertices))

        reference_points = np.array(depth_vertices)
        points = np.array(face_vertices)

        reference_points =\
            apply_transforms_from_obj(reference_points, self._depth_obj)
        points = \
            apply_transforms_from_obj(points, self._face_obj)

        tree = KDTree(reference_points)
        distance, _ = tree.query(points)
        self._distance = distance / np.max(distance)

    def apply_result(self):

        mesh = self._face_obj.data
        if mesh.vertex_colors:
            vertex_color_layer = mesh.vertex_colors.active
        else:
            vertex_color_layer = mesh.vertex_colors.new()

        for poly in mesh.polygons:
            for loop_index in poly.loop_indices:
                loop_vertex_index = mesh.loops[loop_index].vertex_index
                distance = self._distance[loop_vertex_index]
                color = (1.0 - distance, 0.0, distance)
                vertex_color_layer.data[loop_index].color = color

        if 'vertex_material' not in bpy.data.materials:
            material = bpy.data.materials.new('vertex_material')
        else:
            material = bpy.data.materials.get('vertex_material')

        material.use_vertex_color_paint = True
        material.use_vertex_color_light = True
        material.use_shadeless = True

        self._face_obj.active_material = material

        # display material and textures in solid view
        # todo access view3d context
        # bpy.context.space_data.show_textured_solid = True
