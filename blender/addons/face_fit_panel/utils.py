import bpy
import numpy as np
import face_fit.utils.quaternion as quat_utils


def update_transform(
    item,
    location=None,
    rotation_quaternion=None,
    scale=None
):
    if location is not None:
        item['location'] = location
    if rotation_quaternion is not None:
        item['rotation_quaternion'] = rotation_quaternion
    if scale is not None:
        item['scale'] = scale
    return item


def make_empty(
    name,
    location=None,
    rotation_quaternion=None,
    scale=None,
    children=[]
):
    empty = {'type': 'empty', 'name': name}
    update_transform(empty, location, rotation_quaternion, scale)
    if children is not None:
        empty['children'] = children
    return empty


def make_mesh(
    name,
    vertices,
    faces,
    location=None,
    rotation_quaternion=None,
    scale=None
):
    mesh = {
        'type': 'mesh',
        'name': name,
        'vertices': vertices,
        'faces': faces
    }
    update_transform(mesh, location, rotation_quaternion, scale)
    return mesh


def create_bpy_object(
    name,
    data,
    location=None,
    rotation_quaternion=None,
    scale=None
):
    obj = bpy.data.objects.new(name, data)
    if location is not None:
        obj.location = location
    if rotation_quaternion is not None:
        obj.rotation_mode = 'QUATERNION'
        obj.rotation_quaternion = rotation_quaternion
    if scale is not None:
        obj.scale = scale

    obj.show_name = True

    # Link object to scene
    bpy.context.scene.objects.link(obj)

    return obj


def create_bpy_empty(
    type,
    name,
    children=None,
    location=None,
    rotation_quaternion=None,
    scale=None
):
    obj = create_bpy_object(name, None, location, rotation_quaternion, scale)
    obj.empty_draw_type = 'PLAIN_AXES'
    if children is not None:
        for child in children:
            i = create_bpy_item(child)
            i.parent = obj
    return obj


def create_bpy_mesh(
    type,
    name,
    vertices,
    faces,
    location=None,
    rotation_quaternion=None,
    scale=None
):

    # not builtin int type of indices
    # cause blender error
    # todo find function which produce np.int64 output
    for i in range(len(faces)):
        faces[i] = list(map(int, faces[i]))

    # Create mesh and object
    mesh = bpy.data.meshes.new(name + 'Mesh')
    # Create mesh from given vertices, edges, faces. Either edges or
    # faces should be [], or you ask for problems
    mesh.from_pydata(vertices, [], faces)

    # Update mesh with new data
    mesh.update(calc_edges=True)

    return create_bpy_object(name, mesh, location, rotation_quaternion, scale)


def create_bpy_item(options):
    mapping = {
        'mesh': create_bpy_mesh,
        'empty': create_bpy_empty
    }
    return mapping[options['type']](**options)


def bpy_obj_to_dict(obj):
    name = obj.name
    type = obj.type
    location = obj.location[:]
    rotation_quaternion = obj.rotation_quaternion[:]
    scale = obj.scale[:]
    children = [bpy_obj_to_dict(item) for item in obj.children]

    return {
        'name': name,
        'type': type,
        'location': location,
        'rotation_quaternion': rotation_quaternion,
        'scale': scale,
        'children': children
    }


def apply_transforms_from_obj(points, bpy_obj):
    transformed_points = np.array(quat_utils.rotate(
                bpy_obj.rotation_quaternion[:],
                points))
    transformed_points = transformed_points * np.array(bpy_obj.scale[:])
    transformed_points = transformed_points + np.array(bpy_obj.location[:])
    return transformed_points
