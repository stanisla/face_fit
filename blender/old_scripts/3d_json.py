import json
import sys

import face_fit.utils.wavefront as obj_utils
import face_fit.utils.depth as depth_utils
import face_fit.utils.pose as pose_utils

from face_fit.utils.projection import SimpleProjection, FullProjection

from face_fit.minimizer.pose_minimizer import refine_head_position


def update_transform(
    item,
    location=None,
    rotation_quaternion=None,
    scale=None
):
    if location is not None:
        item['location'] = location
    if rotation_quaternion is not None:
        item['rotation_quaternion'] = rotation_quaternion
    if scale is not None:
        item['scale'] = scale
    return item


def make_empty(
    name,
    location=None,
    rotation_quaternion=None,
    scale=None,
    children=[]
):
    empty = {'type': 'empty', 'name': name}
    update_transform(empty, location, rotation_quaternion, scale)
    if children is not None:
        empty['children'] = children
    return empty


def make_mesh(
    name,
    vertices,
    faces,
    location=None,
    rotation_quaternion=None,
    scale=None
):
    mesh = {
        'type': 'mesh',
        'name': name,
        'vertices': vertices,
        'faces': faces
    }
    update_transform(mesh, location, rotation_quaternion, scale)
    return mesh


def make_depth_map_mesh(pose, depth_map, projection_builder=0):
    return make_mesh(
        name='DepthMap',
        vertices=depth_utils.make_vertices(
            pose['cameraMatrix'],
            depth_map,
            projection_builder
        ),
        faces=depth_utils.make_faces(depth_map)
    )


def make_landmarks(pose, depth_map, projection_builder=0):
    landmarks2d = pose_utils.make_landmarks2d(pose)
    landmarks3d = depth_utils.make_points3d(
        pose['cameraMatrix'],
        depth_map,
        landmarks2d,
        projection_builder
    )
    return make_empty(
        name='Landmarks',
        children=[
            make_empty(name='Landmark%02d' % i, location=l)
            for i, l in landmarks3d
        ]
    )


def make_identity_face(pose, tensor):
    mesh = pose_utils.make_identity_face(tensor, pose)
    return make_mesh(
        name='Mesh',
        vertices=mesh['vertices'],
        faces=mesh['faces'],
        location=pose['head']['translation'],
        rotation_quaternion=pose['head']['rotation']
    )


def make_ARKit_face(pose, obj):
    return make_mesh(
        name='ARKitMesh',
        vertices=obj['vertices'],
        faces=[[i - 1 for i in f] for f in obj['faces']],
        location=pose['head']['translation'],
        rotation_quaternion=pose['head']['rotation'],
        scale=[200, 200, 200]
    )


def make_ARKit_face_refined(
    tensor,
    pose,
    obj,
    depth_map,
    projection_builder=0
):
    landmarks2d = pose_utils.make_landmarks2d(pose)
    landmarks3d = depth_utils.make_points3d(
        pose['cameraMatrix'],
        depth_map,
        landmarks2d,
        projection_builder
    )

    indices = map(lambda i: i[0], landmarks3d)
    reference_landmarks = map(lambda i: i[1], landmarks3d)

    all_indices = range(len(landmarks2d))
    missing_indices = set(all_indices) - set(indices)
    missing_indices = list(missing_indices)
    for index in missing_indices:
        reference_landmarks = \
            np.insert(reference_landmarks, index, np.nan, axis=0)

    rotation, translation, scale = refine_head_position(
        tensor,
        pose,
        obj,
        reference_landmarks, indices
    )

    return make_mesh(
        name='ARKitMeshRefined',
        vertices=obj['vertices'],
        faces=[[i - 1 for i in f] for f in obj['faces']],
        location=translation.tolist(),
        rotation_quaternion=rotation.tolist(),
        scale=[200 * scale, 200 * scale, 200 * scale]
    )


def make_ARKit_landmarks(tensor, pose, obj):
    translation = pose['head']['translation']
    rotation = pose['head']['rotation']
    landmarks3d = pose_utils.make_landmarks3d(tensor, pose, obj['vertices'])
    return make_empty(
        name='ARKitLandmarks',
        location=translation,
        rotation_quaternion=rotation,
        scale=[200, 200, 200],
        children=[make_empty(
            name='ARKitLandmark%02d' % int(i),
            location=l,
            scale=[0.005, 0.005, 0.005]
        ) for i, l in enumerate(landmarks3d)],
    )


def make_scene(tensor, pose, obj, depth_map):
    return [make_empty(
        name='Laplacian',
        children=[
            make_identity_face(pose, tensor),
            make_ARKit_face(pose, obj),
            make_ARKit_landmarks(tensor, pose, obj),
            make_empty(name='SimpleProjection', children=[
                make_depth_map_mesh(
                    pose,
                    depth_map,
                    projection_builder=SimpleProjection.Builder
                ),
                make_landmarks(
                    pose,
                    depth_map,
                    projection_builder=SimpleProjection.Builder
                ),
                make_ARKit_face_refined(
                    tensor,
                    pose,
                    obj,
                    depth_map,
                    projection_builder=SimpleProjection.Builder
                ),
            ]),
            make_empty(name='FullProjection', children=[
                make_depth_map_mesh(
                    pose,
                    depth_map,
                    projection_builder=FullProjection.Builder
                ),
                make_landmarks(
                    pose,
                    depth_map,
                    projection_builder=FullProjection.Builder
                ),
                make_ARKit_face_refined(
                    tensor,
                    pose,
                    obj,
                    depth_map,
                    projection_builder=FullProjection.Builder
                ),
            ]),
        ]
    )]


if __name__ == '__main__':
    with open(sys.argv[1], 'r') as f:
        tensor = json.load(f)
    with open(sys.argv[2], 'r') as f:
        pose = json.load(f)
    with open(sys.argv[3], 'r') as f:
        obj = obj_utils.load(f)
    depth = depth_utils.read_depth_map(sys.argv[4])
    print json.dumps(blender.scene.make_scene(tensor, pose, obj, depth))
