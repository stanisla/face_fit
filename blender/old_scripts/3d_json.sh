#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$DIR/../../.py/bin/activate"
python "$DIR/3d_json.py" "$@"
