import json
import os
import subprocess

import bpy


def load_scene(*args):
    process = subprocess.Popen(
        args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    outs, errs = process.communicate()
    if process.returncode != 0:
        print('return code', process.returncode)
        print('---------- stdout ----------\n')
        print(outs.decode('utf-8'))
        print('\n---------- stderr ----------\n')
        print(errs.decode('utf-8'))
        print('\n----------        ----------')
        raise Exception(
            'Return code {} in child process.'.format(process.returncode)
        )
    return json.loads(outs.decode('utf-8'))


def create_object(
    name,
    data,
    location=None,
    rotation_quaternion=None,
    scale=None
):
    obj = bpy.data.objects.new(name, data)
    if location is not None:
        obj.location = location
    if rotation_quaternion is not None:
        obj.rotation_mode = 'QUATERNION'
        obj.rotation_quaternion = rotation_quaternion
    if scale is not None:
        obj.scale = scale

    obj.show_name = True

    # Link object to scene
    bpy.context.scene.objects.link(obj)

    return obj


def create_empty(
    type,
    name,
    children=None,
    location=None,
    rotation_quaternion=None,
    scale=None
):
    obj = create_object(name, None, location, rotation_quaternion, scale)
    obj.empty_draw_type = 'PLAIN_AXES'
    if children is not None:
        for child in children:
            i = create_item(child)
            i.parent = obj
    return obj


def create_mesh(
    type,
    name,
    vertices,
    faces,
    location=None,
    rotation_quaternion=None,
    scale=None
):
    # Create mesh and object
    mesh = bpy.data.meshes.new(name + 'Mesh')
    # Create mesh from given vertices, edges, faces. Either edges or
    # faces should be [], or you ask for problems
    mesh.from_pydata(vertices, [], faces)

    # Update mesh with new data
    mesh.update(calc_edges=True)

    return create_object(name, mesh, location, rotation_quaternion, scale)


def create_item(options):
    mapping = {
        'mesh': create_mesh,
        'empty': create_empty
    }
    return mapping[options['type']](**options)


def main():

    base = os.path.dirname(__file__)
    command = os.path.join(base, '3d_json.sh')
    data = os.path.join(base, '../data')
    tensor = os.path.join(data, 'tensor-apple-cyl-anton.json')
    anton = os.path.join(data, 'Anton_9.05_front_face_even_lighting_r1')
    mesh = os.path.join(anton, 'Meshes/pose_0015.obj')
    json = os.path.join(anton, 'pose_0015.json')
    jpeg = os.path.join(anton, 'pose_0015.jpg')

    scene = load_scene(command, tensor, json, mesh, jpeg)
    for item in scene:
        create_item(item)

    # imported_object = bpy.ops.import_scene.obj(filepath=mesh)


if __name__ == '__main__':
    main()

