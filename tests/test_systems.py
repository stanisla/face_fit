import json

import autograd.numpy as np
import scipy.optimize

import face_fit.utils.wavefront
import face_fit.utils.depth
import face_fit.utils.pose

from face_fit.utils.quaternion import quaternion_to_euler
from face_fit.utils.projection import SimpleProjection

from face_fit.minimizer.systems import Landmarks, RigidTransform


class TestSystems(object):

    def load_data(
            self,
            load_tensor=True,
            load_pose=True,
            load_depth=True,
            load_mesh=True
    ):

        # path to test data
        base_path = '../data/'

        tensor_path = base_path + 'tensor-apple-cyl-anton.json'

        pose_path = base_path + \
            'Anton_9.05_front_face_even_lighting_r1/pose_0015.json'

        image_path = base_path + \
            'Anton_9.05_front_face_even_lighting_r1/pose_0015.jpg'

        mesh_path = base_path + \
            'Anton_9.05_front_face_even_lighting_r1/Meshes/pose_0015.obj'

        result = []

        if load_tensor:
            with open(tensor_path) as f:
                tensor = json.load(f)
                result.append(tensor)

        if load_pose:
            with open(pose_path) as f:
                pose = json.load(f)
                result.append(pose)

        if load_depth:
            depth = face_fit.utils.depth.read_depth_map(image_path)
            result.append(depth)

        if load_mesh:
            with open(mesh_path) as f:
                mesh = face_fit.utils.wavefront.load(f)
                result.append(mesh)

        return result

    def test_rigid_transform_system(self):

        tensor, pose, depth, obj = self.load_data()

        head_rotation = quaternion_to_euler(pose['head']['rotation'])
        head_translation = pose['head']['translation']

        landmarks = Landmarks(tensor['landmarkBarycentricCoordinates'])

        head_vertices = np.array(obj['vertices']) * 200

        head_landmarks = np.array(landmarks(head_vertices))
        depth_landmarks = face_fit.utils.pose.make_landmarks2d(pose)
        depth_landmarks = face_fit.utils.depth.make_points3d(
            pose['cameraMatrix'],
            depth,
            depth_landmarks,
            SimpleProjection.Builder
        )

        indices = list(map(lambda item: item[0], depth_landmarks))

        head_landmarks = head_landmarks[indices]
        depth_landmarks = np.array(list(map(lambda item: item[1], depth_landmarks)))

        landmarks_transform = RigidTransform(head_landmarks)

        def f(x):
            return np.array(
                landmarks_transform(x[0:3], x[3:6], [x[6], x[6], x[6]])
            ).flatten()

        x_start = np.hstack((head_rotation, head_translation, [1]))
        y_target = depth_landmarks.flatten()

        result = scipy.optimize.least_squares(
            lambda x: f(x) - y_target,
            x0=x_start,
            # jac=jacobian(f),
            bounds=(-np.inf, np.inf),
            method='trf',
            ftol=1e-8,
            xtol=1e-8,
            gtol=1e-8,
            x_scale=1.0,
            loss='linear',
            f_scale=1.0,
            diff_step=None,
            tr_solver='lsmr',
            tr_options={},
            # jac_sparsity=jac_sparsity,
            max_nfev=None,
            verbose=0,
            args=(),
            kwargs={}
        )

        assert result.success

    def test_landmarks_system(self):

        tensor, obj = self.load_data(load_pose=False, load_depth=False)

        vertices = np.array(obj['vertices']) * 200
        vertices_flatten = vertices.flatten()
        barycentric_coords = tensor['landmarkBarycentricCoordinates']

        landmarks = Landmarks(barycentric_coords)

        def f(x):
            arg = np.reshape(x, (-1, 3))
            return np.array(landmarks(arg)).flatten()

        x_target = vertices_flatten
        y_target = f(x_target)

        x_start = (vertices - np.array([0.0, 0.0, 15.0])).flatten()

        jac_sparsity = np.zeros((len(y_target), len(x_target)), dtype=int)
        dependencies = landmarks.dependencies()
        for i, indices in enumerate(dependencies):
            for j in indices:
                y_range = list(range(i * 3, (i + 1) * 3))
                x_range = list(range(j * 3, (j + 1) * 3))
                jac_sparsity[y_range, x_range] = 1

        ftol = 1e-8
        xtol = 1e-8
        gtol = 1e-8

        result = scipy.optimize.least_squares(
            lambda x: f(x) - y_target,
            x0=x_start,
            # jac=jacobian(f),
            bounds=(-np.inf, np.inf),
            method='trf',
            ftol=ftol,
            xtol=xtol,
            gtol=gtol,
            x_scale=1.0,
            loss='linear',
            f_scale=1.0,
            diff_step=None,
            tr_solver='lsmr',
            tr_options={},
            jac_sparsity=jac_sparsity,
            max_nfev=None,
            verbose=0,
            args=(),
            kwargs={}
        )

        assert result.success
