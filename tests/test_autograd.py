import autograd.numpy as np
import numpy.testing as np_testing
from autograd import grad


def norm(a):
    return np.sqrt(np.dot(a, a))


class TestAutograd(object):
    def test_dot_product(self):
        g = grad(norm)
        np_testing.assert_array_almost_equal(g([1., 0., 0.]), [1., 0., 0.])
        np_testing.assert_array_almost_equal(g([0., 1., 0.]), [0., 1., 0.])
        np_testing.assert_array_almost_equal(g([0., 0., 1.]), [0., 0., 1.])
        np_testing.assert_array_almost_equal(
            g([9., 12., 20.]),
            [9. / 25., 12. / 25., 20. / 25.]
        )
