.PHONY: help
help:
	cat makefile

.PHONY: clean_modules
clean_modules:
	rm -rf blender/modules/*

.PHONY: install_modules
install_modules:
	pip3 install --requirement blender/requirements.txt --target blender/modules
	cd blender/modules && ln -s ../../face_fit
