import numpy as np


class Projection(object):
    def __init__(self, camera_matrix, screen_size):
        self._camera_matrix = camera_matrix
        self._screen_size = screen_size

        w, h = list(map(float, screen_size))

        self._x0 = -(w / h) + 1 / h
        self._y0 = -1 + 1 / h
        self._dxy = 2 / h

    def _pixel_to_model_coords(self, i, j):
        x = self._x0 + self._dxy * i
        y = -(self._y0 + self._dxy * j)
        return x, y

    def _model_to_pixel_coords(self, x, y):
        i = (x - self._x0) / self._dxy
        j = -(y + self._y0) / self._dxy
        return i, j

    def projection(self, x, y, z):
        pass

    def back_projection(self, i, j, z):
        pass


class SimpleProjection(Projection):
    def __init__(self, camera_matrix, screen_size):
        super(SimpleProjection, self).__init__(camera_matrix, screen_size)

        focal_length = camera_matrix[0]
        f = 1 / focal_length

        self._inv_matrix = np.array([
            [f, 0, 0],
            [0, f, 0],
            [0, 0, 1]
        ])

        self._matrix = np.array([
            [focal_length, 0, 0],
            [0, focal_length, 0],
            [0, 0, 1]
        ])

    def projection(self, x, y, z):
        i, j, w = self._matrix.dot((x, y, -z)) / -z
        assert w == 1
        return super(SimpleProjection, self)._model_to_pixel_coords(i, j)

    def back_projection(self, i, j, z):
        x, y = super(SimpleProjection, self)._pixel_to_model_coords(i, j)
        return self._inv_matrix.dot((z * x, z * y, -z)).tolist()

    class Builder(object):
        @staticmethod
        def make_projection(camera_matrix, screen_size):
            return SimpleProjection(camera_matrix, screen_size)


class FullProjection(Projection):
    def __init__(self, camera_matrix, screen_size):
        super(FullProjection, self).__init__(camera_matrix, screen_size)

        self._camera_matrix = np.array(self._camera_matrix)
        self._camera_matrix = np.reshape(self._camera_matrix, (4, 4))
        self._camera_matrix = np.matrix(self._camera_matrix)
        self._inverted_camera_matrix = np.linalg.inv(self._camera_matrix)

    def projection(self, x, y, z):
        p = np.dot(self._camera_matrix, [x, y, z, 1.0])
        w = p[0, 2]
        i, j = p[0, 0] / w, p[0, 1] / w
        return super(FullProjection, self)._model_to_pixel_coords(i, j)

    def back_projection(self, i, j, z):
        x, y = super(FullProjection, self)._pixel_to_model_coords(i, j)
        w = (np.dot(self._camera_matrix, [0, 0, -z, 1.0]))[0, 2]
        p = np.dot(self._inverted_camera_matrix, [x, y, 1.0, -z / w])
        p = np.squeeze(np.asarray(p))[0:3] * w
        return p.tolist()

    class Builder(object):
        @staticmethod
        def make_projection(camera_matrix, screen_size):
            return FullProjection(camera_matrix, screen_size)
