
def load(file):

    vertices = []
    faces = []

    for line in file:
        parts = line.split(' ')
        if parts[0] is 'v':
            vertex = list(map(float, parts[1:4]))
            vertices.append(vertex)
        elif parts[0] is 'f':
            face = [part.split('/')[0] for part in parts[1:4]]
            face = list(map(int, face))
            faces.append(face)

    return {'vertices': vertices, 'faces': faces}


def save(file, mesh):

    vertices = mesh['vertices']
    faces = mesh['faces']

    for v in vertices:
        file.write('v %f %f %f\n' % (v[0], v[1], v[2]))

    for f in faces:
        file.write('f %d %d %d\n' % (f[0], f[1], f[2]))
