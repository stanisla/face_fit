import itertools
import numpy as np


def make_identity_face(tensor, pose):
    vertices = np.zeros_like(tensor['vertices'][0])

    # make identity
    m = len(pose['identity']['weights'])
    assert m <= len(tensor['vertices'])
    weights = pose['identity']['weights']
    shapes = tensor['vertices'][:m]
    for w, s in itertools.izip(weights, shapes):
        vertices += w * np.array(s)

    # add expressions
    expr_names = tensor['expressions']['names']
    expr_diffs = tensor['expressions']['meshDiffs']
    assert len(expr_names) == len(expr_diffs)
    diffs = {n: np.array(d) for n, d in itertools.izip(expr_names, expr_diffs)}
    for k, w in pose['shapes'].items():
        vertices += w * diffs[k]

    return {
        'vertices': vertices.tolist(),
        'faces': [[i - 1 for i in f] for f in tensor['faces']]
    }


def make_landmarks2d(pose):
    coords = [float(v) / 2 for v in pose['landmarks2d']]
    assert len(coords) % 2 == 0

    landmarks2d = zip(coords[::2], coords[1::2])
    return list(landmarks2d)


def make_landmarks3d(tensor, pose, vertices=None):
    barycentric_coordinates = tensor['landmarkBarycentricCoordinates']
    if vertices is None:
        vertices = make_identity_face(tensor, pose)['vertices']
    vertices = np.array(vertices)

    landmarks3d = []

    for item in barycentric_coordinates:
        points = vertices[np.array(item[0])]
        coeffs = item[1]
        landmark3d = \
            points[0] * coeffs[0] + \
            points[1] * coeffs[1] + \
            points[2] * coeffs[2]
        landmarks3d.append(landmark3d.tolist())

    return landmarks3d
