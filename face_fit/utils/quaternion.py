import math

from pyquaternion import Quaternion


def quaternion_to_euler(quaternion):

    w = quaternion[0]
    x = quaternion[1]
    y = quaternion[2]
    z = quaternion[3]

    roll = math.atan2(2.0 * (x * y + z * w), (x * x - y * y - z * z + w * w))
    yaw = math.atan2(2.0 * (y * z + x * w), (-x * x - y * y + z * z + w * w))
    pitch = math.asin(-2.0 * (x * z - y * w))

    return [yaw * 180.0 / math.pi,
            pitch * 180.0 / math.pi,
            roll * 180.0 / math.pi]


def euler_to_quaternion(euler_angles):

    yaw = euler_angles[0] * math.pi / 180.0
    pitch = euler_angles[1] * math.pi / 180.0
    roll = euler_angles[2] * math.pi / 180.0

    w = math.cos(yaw * 0.5) * math.cos(roll * 0.5) * math.cos(pitch * 0.5) +\
        math.sin(yaw * 0.5) * math.sin(roll * 0.5) * math.sin(pitch * 0.5)
    x = math.sin(yaw * 0.5) * math.cos(roll * 0.5) * math.cos(pitch * 0.5) -\
        math.cos(yaw * 0.5) * math.sin(roll * 0.5) * math.sin(pitch * 0.5)
    y = math.cos(yaw * 0.5) * math.cos(roll * 0.5) * math.sin(pitch * 0.5) +\
        math.sin(yaw * 0.5) * math.sin(roll * 0.5) * math.cos(pitch * 0.5)
    z = math.cos(yaw * 0.5) * math.sin(roll * 0.5) * math.cos(pitch * 0.5) -\
        math.sin(yaw * 0.5) * math.cos(roll * 0.5) * math.sin(pitch * 0.5)

    return [w, x, y, z]


def rotate(quaternion, points):
    quaternion = Quaternion(quaternion)
    rotated_points = list(map(quaternion.rotate, points))
    return rotated_points
