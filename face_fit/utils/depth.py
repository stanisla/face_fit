import math
import numpy as np
import struct

from ..utils.mjpeg import ParseJpeg


def _parse_jpeg(fname):
    SOF0 = 0xff, 0xc0
    EOI = 0xff, 0xd9
    width = None
    height = None
    data = open(fname, 'rb').read()
    for twoBytes, frameStartPos, frameEndPos in ParseJpeg().Parse(data):
        if twoBytes == SOF0:
            height, width = struct.unpack(
                '>HH',
                data[frameStartPos + 5:frameStartPos + 9]
            )
        if twoBytes == EOI:
            return width, height, data[frameEndPos:]


def read_depth_map(fname):
    width, height, extra = _parse_jpeg(fname)

    prefix = '-START OF DEPTH DATA-'
    if extra[:len(prefix)].decode("utf-8") != prefix:
        print(len(extra))
        assert False
    extra = extra[len(prefix):]

    assert width % 2 == 0
    assert height % 2 == 0
    w = int(width / 2)
    h = int(height / 2)
    assert len(extra) % 4 == 0

    assert w * h == len(extra) / 4
    depth = np.empty((w, h), dtype=np.float32)

    for x in range(w):
        for y in range(h):
            z = 4 * (h * (w - 1 - x) + y)
            depth[x, y] = struct.unpack('f', extra[z:z + 4])[0]
    return depth


def assert_eq(a, b):
    try:
        assert a == b
    except AssertionError:
        print('AssertionError: {} != {}'.format(a, b))
        raise


def assert_lte(a, b):
    try:
        assert a <= b
    except AssertionError:
        print('AssertionError: {} > {}'.format(a, b))
        raise


def interpolate(*pairs):
    s = 0
    w = 0
    for a, y in pairs:
        if not np.isnan(y):
            s += a * y
            w += a
    if w == 0:
        return float('nan')
    return s / w


def get_depth(depth, x, y):
    if isinstance(x, int) and isinstance(y, int):
        return depth[x, y]
    if isinstance(x, float) and isinstance(y, float):
        x0 = int(math.floor(x))
        x1 = int(math.ceil(x))
        y0 = int(math.floor(y))
        y1 = int(math.ceil(y))
        ax0 = x1 - x
        ax1 = x - x0
        ay0 = y1 - y
        ay1 = y - y0
        if x0 == x1:
            ax0 = ax1 = 0.5
        if y0 == y1:
            ay0 = ay1 = 0.5
        return interpolate(
            (ax0 * ay0, depth[x0, y0]),
            (ax0 * ay1, depth[x0, y1]),
            (ax1 * ay0, depth[x1, y0]),
            (ax1 * ay1, depth[x1, y1])
        )


def enum_depth_map(depth):
    for index, d in np.ndenumerate(depth):
        if not np.isnan(d):
            yield index, d


def make_indices(depth):
    indices = np.full_like(depth, -1, dtype=int)
    i = 0
    for index, d in enum_depth_map(depth):
        indices[index] = i
        i += 1
    return indices


def make_vertices(camera_matrix, depth, projection_builder):
    projection = projection_builder.make_projection(
        camera_matrix,
        np.shape(depth)
    )
    return [
        projection.back_projection(i, j, d * 200)
        for (i, j), d in enum_depth_map(depth)
    ]


def make_faces(depth):
    faces = []
    indices = make_indices(depth)
    assert_eq(len(indices.shape), 2)
    for i in range(indices.shape[0] - 1):
        for j in range(indices.shape[1] - 1):
            face_indices = [
                indices[i, j],
                indices[i + 1, j],
                indices[i + 1, j + 1],
                indices[i, j + 1]
            ]
            face_indices = [v for v in face_indices if v >= 0]
            if len(face_indices) > 2:
                faces.append(face_indices)
    return faces


def make_points3d(camera_matrix, depth, points2d, projection_builder):
    projection = projection_builder.make_projection(
        camera_matrix,
        np.shape(depth)
    )
    points3d = []
    for i, (x, y) in enumerate(points2d):
        d = get_depth(depth, x, y)
        if not np.isnan(d):
            points3d.append((i, projection.back_projection(x, y, d * 200)))
    return points3d
