import autograd.numpy as np


class Landmark(object):

    def __init__(self, v, c):
        assert len(v) == len(c)
        self._indices = v
        self._coeffs = c

    def __call__(self, vertices):
        landmark = np.array([0., 0., 0.])
        for i in range(len(self._indices)):
            landmark += np.array(vertices[self._indices[i]]) * self._coeffs[i]
        return landmark

    def dependencies(self):
        return [v for v in self._indices]


class Landmarks(object):

    def __init__(self, barycentric_coordinates):
        self._landmarks = [
            Landmark(v, c) for [v, c] in barycentric_coordinates
        ]

    def __call__(self, vertices):
        return [l(vertices) for l in self._landmarks]

    def dependencies(self):
        return [l.dependencies() for l in self._landmarks]


class RigidTransform(object):

    def __init__(self, vertices):
        self._vertices = vertices

    def __call__(self, rotation, translation, scale=None):

        if scale is None:
            scale = np.array([1.0, 1.0, 1.0])

        angles = [r * np.pi / 180.0 for r in rotation]
        rotation_x = np.matrix([
            [1, 0, 0],
            [0, np.cos(angles[0]), np.sin(angles[0])],
            [0, -np.sin(angles[0]), np.cos(angles[0])]
        ])
        rotation_y = np.matrix([
            [np.cos(angles[1]), 0, -np.sin(angles[1])],
            [0, 1, 0],
            [np.sin(angles[1]), 0, np.cos(angles[1])]
        ])
        rotation_z = np.matrix([
            [np.cos(angles[2]), np.sin(angles[2]), 0],
            [-np.sin(angles[2]), np.cos(angles[2]), 0],
            [0, 0, 1]
        ])

        rotation_matrix = rotation_x * rotation_y * rotation_z

        vertices = \
            np.tensordot(self._vertices, rotation_matrix, axes=1) * scale + \
            translation
        return vertices
