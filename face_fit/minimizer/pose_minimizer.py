import scipy.optimize
import numpy as np

from .systems import Landmarks, RigidTransform
from ..utils.quaternion import euler_to_quaternion, quaternion_to_euler


def refine_head_position_landmarks(
        head_landmarks,
        reference_landmarks,
        indices=None,
        initial_translation=None,
        initial_rotation_quat=None,
        initial_scale=None):

    head_translation = [0.0, 0.0, 0.0] if initial_translation is None \
        else initial_translation

    head_rotation = [0.0, 0.0, 0.0] if initial_rotation_quat is None \
        else quaternion_to_euler(initial_rotation_quat)

    head_scale = [1.0] if initial_scale is None \
        else initial_scale

    head_landmarks = np.array(head_landmarks)
    reference_landmarks = np.array(reference_landmarks)

    if indices is not None:
        head_landmarks = head_landmarks[indices]
        reference_landmarks = reference_landmarks[indices]

    landmarks_transform = RigidTransform(head_landmarks)

    def f(x):
        return np.array(
            landmarks_transform(x[0:3], x[3:6], [x[6], x[6], x[6]])
        ).flatten()

    x_start = np.hstack((head_rotation, head_translation, head_scale))
    y_target = np.array(reference_landmarks).flatten()

    result = scipy.optimize.least_squares(
        lambda x: f(x) - y_target,
        x0=x_start,
        # jac=None,
        bounds=(-np.inf, np.inf),
        method='trf',
        ftol=1e-8,
        xtol=1e-8,
        gtol=1e-8,
        x_scale=1.0,
        loss='linear',
        f_scale=1.0,
        diff_step=None,
        tr_solver='lsmr',
        tr_options={},
        # jac_sparsity=None,
        max_nfev=None,
        verbose=0,
        args=(),
        kwargs={}
    )

    return \
        np.array(euler_to_quaternion(result.x[0:3])), \
        result.x[3:6], \
        result.x[6]


def refine_head_position(tensor, pose, obj, reference_landmarks, indices=None):

    head_rotation_quat = pose['head']['rotation']
    head_translation = pose['head']['translation']

    landmarks = Landmarks(tensor['landmarkBarycentricCoordinates'])

    head_vertices = np.array(obj['vertices']) * 200

    head_landmarks = np.array(landmarks(head_vertices))

    return refine_head_position_landmarks(
        head_landmarks=head_landmarks,
        reference_landmarks=reference_landmarks,
        indices=indices,
        initial_translation=head_translation,
        initial_rotation_quat=head_rotation_quat
    )
