

def WriteObj(mesh, path):

    def write_list(f, tag, list_):
        f.write(tag)
        for i in list_:
            f.write(' {}'.format(i))
        f.write('\n')

    vertices = mesh['vertices']
    faces = mesh['faces']

    with open(path, 'w') as f:
        f.write('# Vertices count: {}\n'.format(len(vertices)))
        f.write('# Faces count: {}\n'.format(len(faces)))
        f.write('\n')

        for v in vertices:
            write_list(f, 'v', v)

        for f_ in faces:
            write_list(f, 'f', f_)
